<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 3</title>
</head>
<body>
    <div class="container-fluid">
      <div class="row justify-content-center">
                <div class="card w-50 m-4">
                    <div class="card-header">
                        <h2 style="text-align:center">Division Table</h2>
                        </div>
                        <div class="card-body">
                    <TABLE class="table table-bordered table-hover" BORDER=1>
                    <?php
                      echo "<tr>";
                        echo "<th> </th>";
                        for ($index = 1;$index <=10;$index++)
                            echo("<th>$index</th>");
                        echo("</tr>");

                        for ($index = 1;$index <= 10;$index++){
                            echo("<tr><th>$index</th>");
                            for ($ctr = 1;$ctr <= 10;$ctr++){
                                $result = $ctr / $index;
                                printf("<td>%.2f</td>", $result); 
                            }
                            echo("</tr>\n");
                        }
                    ?>
                    </div>
                    </table>
                    </div>
            </div>
     </div>
</body>
</html>