<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 5</title>
</head>
<body>
 <div class="container-fluid">
      <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card my-3">
                    <div class="card-header">
                        <h3>Write a program to delete the recurring elements inside a sorted list of strings</h3>
                        </div>
                        <div class="card-body">
                    <?php
                        $num_list=array(5,5,6,3,3,3,3,4,1,1,1,1,2);
                        echo "<pre>";
                        echo "Sorted Array<br>";
                        sort($num_list);
                        print_r($num_list);
                        echo "Updated Array<br>";  
                        print_r(array_unique($num_list));
                    ?>
                    </div>
                    </div>
            </div>
     </div>

</body>
</html>