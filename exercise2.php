<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid">
      <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card my-3">
                    <div class="card-header">
                        <h3>Write a program in PHP to find the best deal to purchase the item.</h3>
                        </div>
                        <div class="card-body">
                    <?php
                        $quantity1 = 70;
                        $quantity2 = 100;
                        $price1 = 35;
                        $price2 = 30;
                        echo "<pre>";
                        echo "<h4>First Deal </h4>";
                        echo "Quantity 1 =".$quantity1 ."<br>";
                        echo "Price 1= " . $price1. "<br>";
                        echo "<h4>Second Deal </h4>";
                        echo "Quantity 2 =".$quantity2 ."<br>";
                        echo "Price 2= " . $price2. "<br>";
                        echo "Result: <br>";
                        if($quantity1*$price1 <$quantity2*$price2){
                            echo "The first deal is better than the second deal! You might want to purchase it";
                        }else{
                             echo "The second deal is better than the frist deal! You might want to purchase it";
                    }
                    ?>
                    </div>
                    </div>
            </div>
     </div>

</body>
</html>