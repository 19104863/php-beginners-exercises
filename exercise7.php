<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 7</title>
</head>
<body>
    <div class="container-fluid">
      <div class="row justify-content-center">
                <div class="card w-50 m-5">
                    <div class="card-header">
                        <h6>Write a program to convert a digit into its word counterpart. E.g. 721 - Seven Two One </h6>
                        </div>
                        <div class="card-body">
                            <!-- <form method="post"> -->
                        <div class="form-group">
                    <form action="" method="post">
                            <input type="number" class="form-control" name="number" aria-describedby="emailHelp" placeholder="Enter a number">
                            <small id="emailHelp"  class="form-text text-muted">Convert digit into words.</small><br><br>
                            <button type="submit" name ="post"  class="btn btn-primary">Convert</button>
                            <p></p>
                        </div>
</form>
                    <?php
                      
                        if(isset($_POST['post'])){
                            $input_num = $_POST['number'];
                            $input_num= strval($input_num);
                            $num_length=strlen($input_num);
                            for($index=0;$index<$num_length;$index++){
                                
                                switch($input_num[$index]){
                                case "1":
                                        echo "One ";
                                        break;  
                                 case "2":
                                        echo "Two ";
                                        break;
                                 case "3":
                                        echo "Three ";
                                        break;
                                 case "4":
                                        echo "Four ";
                                        break;
                                 case "5":
                                        echo "Five ";
                                        break;
                                 case "6":
                                        echo "Six ";
                                        break;
                                 case "7":
                                        echo "Seven ";
                                        break; 
                                 case "8":
                                        echo "Eight ";
                                        break;   
                                 case "9":
                                        echo "Nine ";
                                        break;
                                default:
                                        echo "Zero ";           
                                }
                                }
                        }
                    ?>
                    </div>
                    </table>
                    </div>
            </div>
     </div>
</body>
</html>