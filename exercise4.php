<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 4</title>
</head>
<body>
     <div class="container-fluid">
      <div class="row justify-content-center">
                <div class="card w-50 m-5">
                    <div class="card-header">
                        <h6>Write a program to determine if the number is an Armstrong Number. An Armstrong Number is a number such that the sum of the cubes of its digits is equal to the number itself.</h6>
                        </div>
                        <div class="card-body">
                            <form method="post">
                        <div class="form-group">
                            <input type="number" class="form-control" name="number" aria-describedby="emailHelp" placeholder="Enter a number">
                            <small id="emailHelp" class="form-text text-muted">This will determine if the number is an armstrong or not.</small><br><br>
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            <p></p>
                        </div>
                    <?php
                       if(isset($_POST['submit'])) 
                        {     
                        $input_num = $_POST['number'];   
                        $temp=$input_num;
                        $result=0;
                        while($temp!=0){
                            $remainder=$temp%10;
                            $result=$result+($remainder*$remainder*$remainder);
                            $temp=$temp/10;
                        }
                        if( $input_num == $result )  
                        {  
                        print ('<h3>It is an armstrong number.</h3>'); 
                        }else  
                        {  
                         print ('<h3>It is not an armstrong number.</h3>');   
                        }  
                        }  
                    ?>
                    </div>
                    </table>
                    </div>
            </div>
     </div>
</body>
</html>