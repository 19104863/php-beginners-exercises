<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Exercise 1</title>
</head>
<body>
 <!-- Exercise number 1 -->
        <div class="container-fluid">
      <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card my-3">
                    <div class="card-header">
                        <h3>Write a program to loop over the given JSON data. Display the values via loops or recursion.</h3>
                        </div>
                        <div class="card-body">
                        <?php
                                        $json_data = array(
                                "Student1" => array(
                                        "Name" => "John Carg",
                                        "Age" => "17",
                                        "School" => "Ahlcon Public school",
                                ),
                                "Student2" => array(
                                        "Name" => "Smith Soy",
                                        "Age" => "16",
                                        "School" => "St. Marie school",
                                ),
                                "Student3" => array(
                                        "Name" => "Charle Rena  ",
                                        "Age" => "16",
                                        "School" => "St. Columba school",
                                )
                                );

                                $key_value = array_keys($json_data);
                                for($ndx = 0; $ndx < count($json_data); $ndx++) {
                                echo $key_value[$ndx] ."<br><br>";
                                foreach($json_data[$key_value[$ndx]] as $key_values => $value) {
                                        echo $key_values . " : " . $value . "<br>";
                                }
                                echo "<br><br>";
                                }  
                                ?>
<!-- End of exercise number 1 -->  
                                </div>
                        </div>
                </div>
        </div>
  </div> 
</body>
</html>